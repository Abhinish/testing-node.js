const expect = require('expect');

const request = require('supertest');

//For test
var app = require('./server').app;
describe('Server', () =>{

describe('Root', () =>{
    it('Should return the hello Abhinish response', (done) => {
        request(app)
            .get('/')
            .expect(404)
            .expect((res) => {
                expect(res.body).toInclude({
                    errorMessage: 'This is a errorMessage'

                });
            })
            .end(done);
    });
});

describe('Users', () =>{

    it('Should have this User',(done) =>{
        request(app)
            .get('/users')
            .expect(200)
            .expect((res) => {
                expect(res.body).toInclude(
                    {
                        name:'Abhinish',
                        age:23
                    }
                );
            })
            .end(done);
    });

});
});