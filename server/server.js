const express = require('express');

const port = process.env.PORT || 3000;

var app = express();

app.get('/', (req,res) =>{
   res.status(404).send({
       errorMessage:'This is a errorMessage',
       name:'Todo App v1.0'
   });
});

app.get('/users',(req,res) => {
   res.status(200).send( [
        {
           name: 'Abhinish',
           age: 23
       },
        {
           name: 'Abhishek',
           age: 15
       },
       {
           name: 'Kumar',
           age: 28
       },
       {
           name: 'Rajaram',
           age: 43
       }

    ]);
});

app.listen(port, () => {
    console.log(`Server start at the ${port}`);
});

module.exports.app = app;
