const expect = require('expect');
const rewire = require('rewire');

var app = rewire('./app');

//refer the mjackson expect () for more asserts
describe('App', () => {

    var db = {
        saveUser:expect.createSpy()
    };
    app.__set__('db',db);

   it('Should call the spy correctly' , () => {
       var spy = expect.createSpy();
       spy('Abhinish',23);
       expect(spy).toHaveBeenCalledWith('Abhinish',23);
   });

   it('Should call the saveUser with user object', () =>{
      var email = 'araxrock@gmail.com';
      var password = 'abhinish';
      app.handleSignup(email,password);
      expect(db.saveUser).toHaveBeenCalledWith({email,password});
   });
});