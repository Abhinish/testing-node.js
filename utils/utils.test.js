const expect = require('expect');
const utils = require('./utils');

//for describing the test in the node.js block preety much handy when thr module is complex
describe('Utils', () =>{
   describe('Sum',() =>{
       it('should add two numbers', () =>{
           var res = utils.add(33,11);
           expect(res).toBe(44).toBeAn('number');
           // if (res !== 44){
           // throw new Error(`Expected 44 ,but got ${res}`);}
       });

       it('It should async add two Number', (done) =>{
           utils.asycAdd(4,5 ,(sum) => {
               expect(sum).toBe(9).toBeAn('number');
               done();
           });
       });
   });

    it('should give the square', () =>{
        var res = utils.square(50);

        expect(res).toBe(2500).toBeAn('number');

        // if (res !== 2500){
        //     throw new Error(`Square should be 2500 , but got ${res}`);
        // }
    });

    describe('AsyncSquare', () =>{
        it('It should async square two Number', (done) =>{
            utils.asycSquare(4 ,(square) => {
                expect(square).toBe(16).toBeAn('number');
                done();
            });
        });
    });
});

// it('should expect some values', () => {
//    // expect(12).toNotBe(12);
//    //  expect({name: 'Abhinish'}).toNotEqual({name:'Abhinish'});
//    //  expect([2,3,4]).toExclude(1);
//     expect({
//         name:'Abhinish',
//         age:25,
//         location:'Bangalore'
//     }).toExclude({
//         age:23
//     })
// });

//should verify first and last name are set

describe('Name Check', () =>{
    it('Should verify the lastName and firstName', () => {
        var user = {
            location:'bangalore',
            age:25
        };
        user = utils.setName(user,'Abhinish Raj');

        expect(user).toInclude({
            firstName:'Abhinish',
            lastName:'Raj'
        });

    });
});



